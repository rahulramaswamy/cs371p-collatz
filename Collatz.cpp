// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream

#include "Collatz.hpp"

#define CACHE_SIZE 1000000

long cache[CACHE_SIZE] = {};

using namespace std;

long collatz_eval_single (long n);

// ------------
// collatz_read
// ------------

istream& collatz_read (istream& r, int& i, int& j) {
    return r >> i >> j;
}

// ------------
// collatz_eval
// ------------

int collatz_eval (int i, int j) {
    // <your code>
    assert(i > 0 && j > 0 && i < 1000000 && j < 1000000);
    long max_cycle = 1;
    long lower = i < j ? i : j;
    long upper = i < j ? j : i;
    long c = 1;
    for (long n = lower; n <= upper; ++n) {
	// if n is in our cache we can just look it up
	// otherwise we need to calculate it and add it
	// to our cache
        if (cache[n] != 0) {
            c = cache[n];
        } else {
            c = collatz_eval_single(n);
            cache[n] = c;
        }
        if (c > max_cycle)
            max_cycle = c;
    }
    assert(max_cycle > 0);
    return max_cycle;
}

// -------------------
// collatz_eval_single
// -------------------

long collatz_eval_single (long n) {
    assert (n > 0);
    long c = 1;
    while (n > 1) {
	// if n is a valid index and we have calculated
	// the cycle length of n before, we can just
	// use that in our calculation
        if (n < CACHE_SIZE && cache[n] != 0)
            return cache[n] + c - 1;
        if ((n % 2) == 0) {
            n = (n / 2);
        } else {
	    // odd number optimization
            n = (3 * n + 1) / 2;
            ++c;
        }
        ++c;
    }
    assert(c > 0);
    return c;
}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& w, int i, int j, int v) {
    w << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& r, ostream& w) {
    int i;
    int j;
    while (collatz_read(r, i, j)) {
        const int v = collatz_eval(i, j);
        collatz_print(w, i, j, v);
    }
}
